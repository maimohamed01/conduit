import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path:'',redirectTo:'articles',pathMatch:'full'},
  {path:'articles',loadChildren: () => import('./components/articles/articles.module').then(m => m.ArticlesModule)},
  {path:'new-article',loadChildren:()=>import('./components/new-article/new-article.module').then(m => m.NewArticleModule)},
  {path:'article-details/:id',loadChildren:()=>import('./components/article-details/article-details.module').then(m => m.ArticleDetailsModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
